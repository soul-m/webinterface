# SoulM - SystemConfig

## Available Modules
 * UpmpdCliQobuz
 * UpmpdCliTidal
 * SystemCtl
 * NetworkInterfaces
 * Fstab
 * Arm
 * HighresAudio
 * Update Funktion
 * Speicherplatz ansicht
 
## Coming Modules
 * Backup
 * HighResAudio
 
## Konfiguration Marker

```
# start-automatic-generation-upmpd_tidal
# end-automatic-generation-upmpd_tidal

# start-automatic-generation-upmpd_qobuz
# end-automatic-generation-upmpd_qobuz

# start-automatic-generation-upmpd_highresaudio
# end-automatic-generation-upmpd_highresaudio

# start-automatic-generation-arm
# end-automatic-generation-arm

# start-automatic-generation-network_interfaces
# end-automatic-generation-network_interfaces
```

Siehe auch [System Dokumentation](/soul-m/system/wikis)

# Git CLI

> Dokumentation kommt noch

