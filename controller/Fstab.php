<?php

require_once "MainController.php";

class Fstab extends MainController {
    public $module_name = "fstab";
    public $template_name = "templates/fstab.html";

    public function save(){
        $content = $this->get_interfaces();

        // Old
        preg_match_all('/#\s*start-automatic-generation(.*)\n#\s*end-automatic-generation/s', $content, $matches);

        $new_data = "# start-automatic-generation";
        $new_data .= $this->new_entry."\n";
        $new_data .= "# end-automatic-generation";

        $data = preg_replace('/#\s*start-automatic-generation(.*)\n#\s*end-automatic-generation/s', $new_data, $content);

        $this->write_interfaces($data);

        $entry = $this->get_entry();
        $this->entry = $entry[0];
        $this->new_entry = $entry[0];
        $this->comment = $entry[1];
    }

    public function get_path(){
        return "/".shortcodes\joinPaths(Settings::$fs_path, "etc/fstab");
    }

    public function post(){

        $this->get_default("server");
        $this->get_default("path");
        $this->get_default("username");
        $this->get_default("password");
        $this->get_default("credentials_file");

        $items = [];

        $items["uid"] = 1000;
        $items["gid"] = 1000;

        if($this->get_default("username", false) &&
            $this->get_default("password", false) &&
            !$this->get_default("credentials_file")){

            $items["user"] = null;
            $items["username"] = $this->get_default("username", false);
            $items["password"] = $this->get_default("password", false);
        }


        if($this->get_default("credentials_file", false)) {
            $items["credentials_file"] = $this->get_default("credentials_file", false);
        }

        $options = "";
        foreach ($items as $key => $value){
            if($value == null){
                $options .= $key.",";
            }else{
                $options .= $key."=".$value.",";
            }
        }

        $options = substr($options, 0, -1);


        $data_string = "//".$this->get_default("server")."/".$this->get_default("path")." /mnt/nas cifs ".$options. " 0 0";


        

        $this->view->request["msg"]->addMessage(msg::SUCCESS, "NAS Anbindung", "Der Eintrag wurde Erfolgreich hinzugefügt");
        file_put_contents($this->get_path(), $data_string."\n", FILE_APPEND);
        
        shell_exec('mkdir -p /mnt/nas ');
        shell_exec('mount -a');
    }





}
?>