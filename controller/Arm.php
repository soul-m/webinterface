<?php

require_once "MainController.php";
require_once "ConfController.php";

class Arm extends ConfController {
    public $module_name = "arm";
    public $template_name = "templates/arm.html";

    public function get_path()
    {
        return "/".shortcodes\joinPaths(Settings::$fs_path, "opt/arm_default/.abcde.conf");
    }
    protected function get_context_data(){
        if(file_exists("/".shortcodes\joinPaths(Settings::$fs_path, "opt/arm"))){
            $this->context["status"] = "an";
        }else{
            $this->context["status"] = "aus";
        }
        return $this->context;
    }


    public function get(){
        if(is_dir("/".shortcodes\joinPaths(Settings::$fs_path, "opt/arm_default"))){
            return $this->getModule();
        }else{
            $this->view->request["msg"]->addMessage(msg::WARNING, $this->module_name, "arm_default does not exists");
        }
    }

    public function post(){

        if($this->get_default("toggle") && (!$this->get_default("format", false) || !$this->get_default("format", false) == "")){
            syslog(LOG_DEBUG, "ARM status is ". $this->comment);
            $this->toggle();
        }

        if($this->get_default("format", false)){
            $this->change_format();

        }

        $this->save();
    }

    public function change_format(){
        $this->view->request["msg"]->addMessage(msg::SUCCESS, "CD-Ripping", "Der Eintrag wurde erfolgreich auf ".$this->get_default("format")." geändert");
        $this->new_entry = $this->edit_param("OUTPUTTYPE", $this->get_default("format"));
    }

    public function edit_param($key, $value){
        return preg_replace('/(#?OUTPUTTYPE=")(.*)(")/', '$1'.$value.'$3', $this->new_entry);
    }


    public function toggle(){
        $path = "/".shortcodes\joinPaths(Settings::$fs_path, "opt/");

        //var_dump(file_exists("/".shortcodes\joinPaths($path, "arm")));

        if(file_exists("/".shortcodes\joinPaths($path, "arm"))){
            unlink("/".shortcodes\joinPaths($path, "arm"));
            $this->view->request["msg"]->addMessage(msg::SUCCESS, "CD-Ripping", "wurde deaktiviert.");
        }else{
            symlink("/".shortcodes\joinPaths($path, "arm_default"), "/".shortcodes\joinPaths($path, "arm"));
            $this->view->request["msg"]->addMessage(msg::SUCCESS, "CD-Ripping", " wurde  aktiviert.");
        }
    }
}
?>