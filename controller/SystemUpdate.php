<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 14.02.18
 * Time: 18:40
 */

require_once __DIR__."/../contrib/Helper.php";

class SystemUpdate {

    public function __construct() {
        $this->current = [
            "branch_name"=>$this->get_branch(),
            "commit_uuid"=>$this->get_commit(),
            "tag_name"=>$this->get_tag()
        ];

    }

    public function get_branch(){
        return $this->execute_git_command(["--get-branch"]);
    }

    public function get_commit(){
        return $this->execute_git_command(["--get-commit"]);
    }

    public function get_tag(){
        return $this->execute_git_command(["--get-tag"]);
    }

    public function do_fetch($remote=null, $branch=null){

        $branch = Helper::set_default($branch, $this->current["branch_name"]);
        $remote = Helper::set_default($remote, "origin");

        return $this->execute_git_command(["--remote", $remote, "--branch", $branch, "--fetch"]);
    }

    public function do_pull($remote=null, $branch=null){

        //$branch = Helper::set_default($branch, $this->current["branch_name"]);
        $remote = Helper::set_default($remote, "origin");
        if(empty($branch)){
            return $this->execute_git_command(["--remote", $remote, "--pull"]);
        }else{

            return $this->execute_git_command(["--remote", $remote, "--branch", $branch, "--pull"]);
        }
    }

    public function do_reset($commit=null){

        if(empty($commit)){
            throw new Exception("No reset parameter given");
        }

        return $this->execute_git_command(["--reset", $commit]);
    }

    public function do_checkout($checkout=null){
        if(empty($checkout)){
            throw new Exception("No checkout parameter given");
        }

        return $this->execute_git_command(["--checkout", $checkout]);
    }

    public function get_remote_commit($remote=null, $branch=null){
        $branch = Helper::set_default($branch, $this->current["branch_name"]);
        $remote = Helper::set_default($remote, "origin");

        return $this->execute_git_command(["--remote", $remote, "--get-remote-commit", "--branch", $branch]);
    }

    public function get_remote_tags() {

        $uncleaned_tag_string = $this->execute_git_command(["--get-remote-tags"]);
        $uncleaned_tag_array = explode(";", $uncleaned_tag_string);

        function clean_tags($var){
            $result = [];
            if(substr($var, 0, 1) == "v") {
                if((bool)preg_match('/v\d+.\d+.\d+/', $var, $result)){
                    return true;
                }
            }
            return false;
        }
        return array_filter($uncleaned_tag_array, "clean_tags");
    }

    public function execute_git_command($options){

        $cmd_string = join(" ", $options);
        //echo "Execute ./git.bash ".$cmd_string."<br>";
        $command_result = rtrim(shell_exec("./git.bash ".$cmd_string));
        //echo "Result of ./git.bash ".$cmd_string."<br><pre>$command_result</pre><br>";

        return $command_result;
    }


    public function compare_tags($current_tag, $tag_list){
        $new_tag = $current_tag;
        foreach($tag_list as $key => $value){
            if(version_compare($new_tag, $value, "<")){
                $new_tag = $value;
            }
        }
        return $new_tag;
    }


}