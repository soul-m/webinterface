<?php

require_once "ConfController.php";

class UpmpdcliTidal extends ConfController {
    public $module_name = "upmpd_tidal";
    public $template_name = "templates/upmpdcli_tidal.html";

    public function get_path(){
        return "/".shortcodes\joinPaths(Settings::$fs_path, "etc/upmpdcli.conf");
    }

    protected function get_context_data(){
        if($this->comment){
            $this->context["status"] = "Aktiv";
        }else{
            $this->context["status"] = "Inaktiv";
        }
        return $this->context;
    }

    public function post(){
        if($this->get_default("toggle") && !$this->get_default("user", false) && !$this->get_default("password", false)){
            $this->toggle();
        }

        if($this->get_default("user") && $this->get_default("password", false)){
            $this->view->request["msg"]->addMessage(msg::SUCCESS, "Tidal", "Username und Passwort Erfolgreich geändert");

            $this->change_username();
            $this->change_password();
        }


        if($this->get_default("quality", "false") != "false"){
            $this->change_quality();
        }

        $this->save();
        shell_exec('systemctl restart upmpdcli.service');

    }

    public function change_username(){
        $this->new_entry =$this->edit_param("tidaluser", " ".$this->get_default("user"));
    }

    public function change_password(){
        $this->new_entry =$this->edit_param("tidalpass", " ".$this->get_default("password"));
    }


    public function change_quality(){
        $this->new_entry =$this->edit_param("tidalquality", " ".$this->get_default("quality"));
    }


    public function edit_param($key, $value){
        return preg_replace('/(#?'.$key.'\s*=)(.*)/', '$1'.$value.'$3', $this->new_entry);
    }


    public function toggle(){

        if(!$this->comment){
            $this->remove_block_comment();
            $this->view->request["msg"]->addMessage(msg::SUCCESS, "Tidal", "Die Tidal Einstellungen sind aktiviert.");
        }else{
            $this->add_block_comment();
            $this->view->request["msg"]->addMessage(msg::SUCCESS, "Tidal", "Die Tidal Einstellungen sind deaktiviert.");
        }
    }
}
?>