<?php

require_once "ConfController.php";

class UpmpdcliHighresaudio extends ConfController {
    public $module_name = "upmpd_highresaudio";
    public $template_name = "templates/upmpdcli_highresaudio.html";

    public function get_path(){
        return "/".shortcodes\joinPaths(Settings::$fs_path, "etc/upmpdcli.conf");
    }

    protected function get_context_data(){

        if($this->comment){
            $this->context["status"] = "Aktiv";
        }else{
            $this->context["status"] = "Inaktiv";
        }

        return $this->context;
    }

    public function post(){
        if($this->get_default("toggle") && !$this->get_default("user", false) && !$this->get_default("password", false)){
            $this->toggle();
        }

        if($this->get_default("user") && $this->get_default("password", false)){
            $this->view->request["msg"]->addMessage(msg::SUCCESS, "HighresAudio", "Username und Passwort Erfolgreich geändert");

            $this->change_username();
            $this->change_password();
        }

        $this->save();
        shell_exec('systemctl restart upmpdcli.service');

    }

    public function change_username(){
        $this->new_entry =$this->edit_param("highresaudiouser", " ".$this->get_default("user"));
    }

    public function change_password(){
        $this->new_entry =$this->edit_param("highresaudiopass", " ".$this->get_default("password"));
    }

    public function edit_param($key, $value){
        return preg_replace('/(#?'.$key.'\s*=)(.*)/', '$1'.$value.'$3', $this->new_entry);
    }


    public function toggle(){

        if(!$this->comment){
            $this->remove_block_comment();
            $this->view->request["msg"]->addMessage(msg::SUCCESS, "HighresAudio", "Die HighresAudio Einstellungen sind aktiviert.");
        }else{
            $this->add_block_comment();
            $this->view->request["msg"]->addMessage(msg::SUCCESS, "HighresAudio", "Die HighresAudio Einstellungen sind deaktiviert.");
        }
    }
}
?>
