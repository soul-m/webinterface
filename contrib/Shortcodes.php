<?php

namespace shortcodes;

function resolve_attr($obj, $attr_list){

    if(count($attr_list) <= 1) {
        $foo = extract_attr($obj, $attr_list[0]);
        return $foo;
    }

    return resolve_attr(extract_attr($obj, $attr_list[0]), array_splice($attr_list, 1));

}

function extract_attr($obj, $attr){

    if(isset($obj->$attr)){
        return $obj->$attr;
    }elseif(isset($obj[$attr])){
        return $obj[$attr];
    }
    throw new \Exception("No Attribute found (TemplateRender)");
}

function render($content, $context){
    gettype($content);

    //$re = '/{{\s+(.*)\s+}}/U';
      $re = '/{{\s+(.*)\s+}}/U';    
//$string "\$"
    preg_match_all($re, $content, $matches, PREG_SET_ORDER, 0);

    foreach($matches as $key => $value){

        $splitted = explode(".", $value[1]);

        $result = "";
        if(isset($context[$splitted[0]])){
            if(count($splitted) > 1){
                $result = resolve_attr(extract_attr($context, $splitted[0]), array_splice($splitted, 1));
            }else{
                $result = extract_attr($context, $splitted[0]);
            }

            $content = str_replace($value[0], $result, $content);
        }
    }

    return $content;
}

function get_template($filename){
    return file_get_contents($filename);
}

function joinPaths() {
    $args = func_get_args();
    $paths = array();
    foreach ($args as $arg) {
        $paths = array_merge($paths, (array)$arg);
    }
//create_function('$p', 'return trim($p, "/");')
    $paths = array_map(function($p){
        return trim($p, "/");
    }, $paths);
    $paths = array_filter($paths);
    return join('/', $paths);
}