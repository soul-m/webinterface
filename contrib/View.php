<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 23.10.17
 * Time: 12:33
 */


// Import class for flash messages (Mitteilung an den user über geschehnisse)
require "FlashMessage.php";

class View{

    protected $context;

    function __construct()
    {
        // Create Request Attribute
        $this->request = [
            // Call Method
            "method"=> strtolower($_SERVER['REQUEST_METHOD']),
            // Object for Flash Messags
            "msg"=>new FlashMessage()
        ];

        // add request attribute to render context
        $this->context = [
            "request"=>$this->request
        ];
    }

    // Unterscheidet zwischen post und get und ruft eine gleichnahmige methode auf
    public function dispatch(){

        if($this->request["method"] == "get"){
            // Call "get" method when HTTP Method type is GET
            return shortcodes\render($this->get(), $this->get_context_data());
        }elseif ($this->request["method"] == "post"){
            // Call "post" method when HTTP Method type is POST
            return shortcodes\render($this->post(), $this->get_context_data());
        }
    }

    // Kann benutzt werden um render context zu generieren
    protected function get_context_data(){
        return $this->context;
    }


}